<?php
$host = "localhost";
$user = "root";
$pass = "ardi";
$dbss = "promoayam";

$db = new mysqli($host, $user, $pass, $dbss);

function jenisAyam($jenis)
{
	if($jenis == 'T')
		return "Ayam Petelur";
	else 
		return "Ayam Potong";
}

function tglIndo($tanggal)
{
	$taketgl = substr($tanggal, 0,10);
	$takejam = substr($tanggal, 11, 18);
	$tahun = substr($taketgl, 0,4);
	$bulan = substr($taketgl, 5,2);
	$tanggal = substr($taketgl, 8,2);

	if($bulan=="01") $bulan = "Januari";
	if($bulan=="02") $bulan = "Februari";
	if($bulan=="03") $bulan = "Maret";
	if($bulan=="04") $bulan = "April";
	if($bulan=="05") $bulan = "Mei";
	if($bulan=="06") $bulan = "Juni";
	if($bulan=="07") $bulan = "Juli";
	if($bulan=="08") $bulan = "Agustus";
	if($bulan=="09") $bulan = "September";
	if($bulan=="10") $bulan = "Oktober";
	if($bulan=="11") $bulan = "November";
	if($bulan=="12") $bulan = "Desember";

	$tgl = $tanggal." ".$bulan." ".$tahun." ".$takejam;

	return $tgl;
}

function uangIndo($angka)
{
	return "Rp ".number_format($angka, 2, ",", ".");
}