<?php include('config/db.php');?>
<?php 
session_start();
if(empty($_SESSION['idloginuser'])) header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li><a href="modalpotonglist.php">Penjualan Daging</a></li>
             <li><a href="modalpetelurlist.php">Penjualan Telur</a></li>
          </ul>
          <ul class="nav nav-sidebar">
             <li><a href="settingharga.php">Form Setting Harga</a></li>
             <li><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
          </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <canvas id="mySemingguTelur" height="100px"></canvas>
          <hr>

          <canvas id="mySemingguDaging" height="100px"></canvas>
          <hr>
          
        </div>

    <?php include('libs/foot.php');?>

    <?php
    /*************************************************************************/
    $mundur = date('Y-m-d', strtotime("-4 day"));
    $maju   = date('Y-m-d', strtotime("+2 day"));
    
    $data0 = array();
    $sql0 = $db->query("SELECT * FROM modal_telur");
    while ($dd0 = $sql0->fetch_assoc()) {
      $dt = array();
      $sql1 = $db->query("SELECT SUM(d.subtotal) as total, t.tanggal 
            FROM transaksidetail d JOIN transaksi t ON t.idtransaksi = d.idtransaksi 
            WHERE t.tanggal BETWEEN '".$mundur."%' AND '".$maju."%' AND d.periode = '".$dd0['periode']."'
            GROUP BY DATE_FORMAT(t.tanggal, '%Y-%m-%d')
          ");

      while ($dd1 = $sql1->fetch_assoc()) {
        $dt[] = $dd1['total'];
        $pecah = explode(" ", $dd1['tanggal']);
      }

      $randomr    = rand(100, 254);
      $randomg    = rand(100, 254);
      $randomb    = rand(100, 254);
      $warna    = 'rgba('.$randomr.', '.$randomg.', '.$randomb.', 1)';

      array_push($data0, array(
          'label'=>$dd0['periode'], 
          'borderColor'=>$warna,
          'backroundColor'=>$warna,
          'fill'=>false,
          'data'=>$dt
        )
      );
    }

    $label0 = array();
    $sql2 = $db->query("SELECT SUM(d.subtotal) as total, t.tanggal
            FROM transaksidetail d JOIN transaksi t ON t.idtransaksi = d.idtransaksi 
            WHERE t.tanggal BETWEEN '".$mundur."%' AND '".$maju."%' AND d.periode LIKE 'T%'
            GROUP BY DATE_FORMAT(t.tanggal, '%Y-%m-%d')");
    while ($dd = $sql2->fetch_assoc()) {
      $pecah = explode(" ", $dd['tanggal']);
      array_push($label0, $pecah[0]);
    }
    /*************************************************************************/

    /*************************************************************************/
    $mundur = date('Y-m-d', strtotime("-4 day"));
    $maju   = date('Y-m-d', strtotime("+2 day"));
    
    $data00 = array();
    $sql00 = $db->query("SELECT * FROM modal_daging");
    while ($dd00 = $sql00->fetch_assoc()) {
      $dt = array();
      $sql1 = $db->query("SELECT SUM(d.subtotal) as total, t.tanggal 
            FROM transaksidetail d JOIN transaksi t ON t.idtransaksi = d.idtransaksi 
            WHERE t.tanggal BETWEEN '".$mundur."%' AND '".$maju."%' AND d.periode = '".$dd00['periode']."'
            GROUP BY DATE_FORMAT(t.tanggal, '%Y-%m-%d')
          ");

      while ($dd1 = $sql1->fetch_assoc()) {
        $dt[] = $dd1['total'];
        $pecah = explode(" ", $dd1['tanggal']);
      }

      $randomr    = rand(100, 254);
      $randomg    = rand(100, 254);
      $randomb    = rand(100, 254);
      $warna    = 'rgba('.$randomr.', '.$randomg.', '.$randomb.', 1)';

      array_push($data00, array(
          'label'=>$dd00['periode'], 
          'borderColor'=>$warna,
          'backroundColor'=>$warna,
          'fill'=>false,
          'data'=>$dt
        )
      );
    }

    $label00 = array();
    $sql22 = $db->query("SELECT SUM(d.subtotal) as total, t.tanggal
            FROM transaksidetail d JOIN transaksi t ON t.idtransaksi = d.idtransaksi 
            WHERE t.tanggal BETWEEN '".$mundur."%' AND '".$maju."%' AND d.periode LIKE 'D%'
            GROUP BY DATE_FORMAT(t.tanggal, '%Y-%m-%d')");
    while ($dd = $sql22->fetch_assoc()) {
      $pecah = explode(" ", $dd['tanggal']);
      array_push($label00, $pecah[0]);
    }
    /*************************************************************************/
    ?>

    <script>
    var ctx = document.getElementById("mySemingguTelur").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: <?php echo json_encode($label0);?>,
        datasets: <?php echo json_encode($data0);?>
      },
      options: {
        responsive: true,
        hoverMode: 'index',
        title:{
          display: true,
          text:'Penjualan Telur Minggu Ini'
        },
        scales: {
          yAxes: [{
            type: 'linear',
            display: true,
            ticks: {
              beginAtZero:true
            },
          }]
        }
      }
    });

    var ctx = document.getElementById("mySemingguDaging").getContext('2d');
    var myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: <?php echo json_encode($label00);?>,
        datasets: <?php echo json_encode($data00);?>
      },
      options: {
        responsive: true,
        hoverMode: 'index',
        title:{
          display: true,
          text:'Penjualan Daging Minggu Ini'
        },
        scales: {
          yAxes: [{
            type: 'linear',
            display: true,
            ticks: {
              beginAtZero:true
            },
          }]
        }
      }
    });
    </script>
    
  </body>
</html>
