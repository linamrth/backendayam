<?php include('config/db.php');?>
<?php 
session_start();
if(!empty($_SESSION['idloginuser'])) header('location: index.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">
    <title>Warung Bejan</title>

    <link rel="stylesheet" type="text/css" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">

    <link href="dist/css/bootstrap.min.css" rel="stylesheet">

    <style type="text/css">
      @import url(http://fonts.googleapis.com/css?family=Roboto:400);
      body {
        background-image: url('img/ayam1.jpg');
        height: 100%;
        width: 100%;
        background-repeat: no-repeat;
        background-size: cover;
        -webkit-font-smoothing: antialiased;
        font: normal 14px Roboto,arial,sans-serif;
      }

      .container {
          padding: 25px;
          position: fixed;
      }

      .form-login {
          background-color: #EDEDED;
          padding-top: 10px;
          padding-bottom: 20px;
          padding-left: 20px;
          padding-right: 20px;
          border-radius: 15px;
          border-color:#d2d2d2;
          border-width: 5px;
          box-shadow:0 1px 0 #cfcfcf;
      }

      h4 { 
       border:0 solid #fff; 
       border-bottom-width:1px;
       padding-bottom:10px;
       text-align: center;
      }

      .form-control {
          border-radius: 10px;
      }

      .wrapper {
          text-align: center;
      }
    </style>
  </head>

  <body>

    <br><br><br><br>
    <div class="container">
      <div class="row">
        <div class="col-md-offset-5 col-md-3">
          <div class="form-login">
            <?php
            if(isset($_POST['btnLogin'])){
              $user = $_POST['username'];
              $pass = $_POST['password'];

              $sql = $db->query("SELECT p.*, l.* FROM pegawai p JOIN pegawailogin l ON p.idpegawai = l.idpegawai WHERE l.username = '".$user."' AND l.password = '".$pass."'");

              if($sql->num_rows == 1){
                $data = $sql->fetch_assoc();
                session_start();
                $_SESSION['idloginuser'] = $data['idpegawai'];

                $db->query("UPDATE pegawailogin SET last_login = '".date("Y-m-d H:i:s")."' WHERE idpegawai = '".$data['idpegawai']."'");

                header('location: index.php');
              }else{
                $echo = "<h5 class='text-danger'>Username atau password salah.</h5>";
              }
            }
            else {
              $echo = "<h4>Welcome back.</h4>";
            }
            ?>

            <form method="post" action="login.php">
              
            <img src="img/ayam2.jpg" class="img-responsive" alt="" />
            <?php echo $echo;?>
            
            <input required name="username" type="text" id="userName" class="form-control input-sm chat-input" placeholder="username" />
            </br>
            <input required name="password" type="password" id="userPassword" class="form-control input-sm chat-input" placeholder="password" />
            </br>
            
            <div class="wrapper">
              <span class="group-btn">     
                  <button type="submit" name="btnLogin" class="btn btn-primary">login <i class="fa fa-sign-in"></i></button>
              </span>
            </div>

            </form>

          </div>
        </div>
      </div>
    </div>

  </body>
</html>
