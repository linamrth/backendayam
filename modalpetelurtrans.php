<?php include('config/db.php');?>
<?php 
session_start();
if(empty($_SESSION['idloginuser'])) header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li><a href="modalpotonglist.php">Penjualan Daging</a></li>
             <li><a href="modalpetelurlist.php">Penjualan Telur</a></li>
          </ul>
          <ul class="nav nav-sidebar">
             <li><a href="settingharga.php">Form Setting Harga</a></li>
             <li><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h3 class="page-header">Form Transaksi</h3>
          <?php
          $dtuser = $db->query("SELECT p.* FROM pegawai p JOIN pegawailogin WHERE p.idpegawai = '".$_SESSION['idloginuser']."'")->fetch_assoc();
          ?>
          <form class="form-vertical" action="modalpetelurtransproses.php" method="post">
            <input type="hidden" name="pegawai" value="<?php echo $dtuser['idpegawai'];?>">
            <div class="col-sm-4">
              <div class="form-group">
                <label class="control-label" for="pwd">Nama Customer : </label>
                <input required autofocus name="atasnama" type="text" class="form-control" Placeholder="input nama customer" id="nama">
              </div>

              <hr>
              <div class="form-group">
                <label class="control-label" for="jenis">Jenis Produk : </label>     
                <select class="form-control" id="jenisproduk">
                  <option value="">-pilih produk-</option>
                  <?php $prd = $db->query("SELECT * FROM jenis_produk WHERE kategori = 'T'");?>
                  <?php while($dd = $prd->fetch_assoc()){?>
                    <option value="<?php echo $dd['idjenisproduk'].'_'.$dd['namaproduk'].'_'.$dd['hargaproduk'];?>"><?php echo $dd['namaproduk'].' - '.uangIndo($dd['hargaproduk']).'/kg';?></option>
                  <?php }?>
                </select>
              </div>
              <div class="form-group">
                <label class="control-label " for="jenis">Berapa Kilo : </label>    
                <input id="berapakilo" type="text" class="form-control" Placeholder="berapa kilo" id="hargaperkilo">
              </div>              
              <a href="#" id="tambahKeranjang" class="btn btn-primary">
                <i class="glyphicon glyphicon-plus"></i> 
                Tambah
              </a>
              <a href="#" id="hapusKeranjang" class="btn btn-danger">
                <i class="glyphicon glyphicon-remove"></i> 
                Hapus
              </a>

              <input type="hidden" name="periode" value="<?php echo $_GET['periode'];?>">
              <input type="hidden" name="temptotal" id="temptotal" value="0" />
              <input type="hidden" name="tempkg" id="tempkg" value="0" />
              
              <hr>
              <div class="form-group">
                <label class="control-label " for="jenis">Uang Bayar : </label>    
                <input id="bayar" name="bayar" type="text" class="form-control" Placeholder="bayar total" >
              </div>
              <div class="form-group">
                <label class="control-label " for="jenis">Uang Kembali : </label>    
                <input id="kembali" name="kembali" type="text" class="form-control" Placeholder="bayar total" >
              </div>              
              <button type="submit" name="btnLanjut" class="btn btn-success col-sm-12 btn-lg">
                <i class="glyphicon glyphicon-check"></i>
                Lanjut Pembayaran 
              </button>
            </div>
          
            <div class="col-sm-8">
              <div class="panel panel-primary">
                <div class="panel-heading"><i class="glyphicon glyphicon-info"></i> Informasi Transaksi</div>
                <div class="panel-body">
                  <table id="tabelTransaksi" class="table table-striped">
                    <thead>
                      <tr>
                        <th>Barang</th>
                        <th>Harga</th>
                        <th>Kg</th>
                        <th>Sub Total</th>
                      </tr>
                    </thead>
                    <tbody></tbody>
                  </table>

                  <p id="totalKeseluruhan"></p>
                </div>
              </div>
            </div>
          </form>  
        </div>
      </div>
    </div>

    <?php include('libs/foot.php');?>
    <script type="text/javascript">
      $(function(){

        $("#tambahKeranjang").click(function(e){
          e.preventDefault();
          var idproduk  = $("#jenisproduk").val();
          var pecah     = idproduk.split('_');
          var kilonya   = $("#berapakilo").val();
          var hitung    = pecah[2] * kilonya;
          
          var totalall  = $("#temptotal").val();
          var totalkg  = $("#tempkg").val();  

          if(idproduk.length == 0 || kilonya.length == 0){
            alert("Pilih produk dan beli berapa kilo.");
          } else {
            var html      = "<tr>";
            html += "<td><input type='text' name='jnsprd[]' value='"+pecah[1]+"' class='form-control' /></td>";
            html += "<td><input type='text' name='hrgprd[]' value='"+pecah[2]+"' class='form-control'/></td>";
            html += "<td><input type='text' name='kiloprd[]' value='"+kilonya+"' class='form-control'/></td>";
            html += "<td><input type='text' name='subtotal[]' value='"+hitung+"' class='form-control'/></td>";
            html += "</tr>";

            $("#tabelTransaksi > tbody").append(html);
            var hitunglagi = eval(hitung) + eval(totalall);
            $("#temptotal").val(hitunglagi);
            var hitungkg = eval(kilonya) + eval(totalkg);
            $("#tempkg").val(hitungkg);
            $("#totalKeseluruhan").html('Total Bayar : <b>'+hitunglagi+'</b>');
          }

        })

        $("#hapusKeranjang").click(function(e){
          e.preventDefault();

          $("#tabelTransaksi > tbody > tr:last").remove();
        })

        $("#bayar").keyup(function(e){
          e.preventDefault();

          var total = $("#temptotal").val();
          var uang = $(this).val();
          var hitungkembali = eval(uang)-eval(total);
          $("#kembali").val(hitungkembali);
        })
      })
    </script>
  </body>
</html>
