<?php include('config/db.php');?>
<?php 
session_start();
if(empty($_SESSION['idloginuser'])) header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li><a href="modalpotonglist.php">Penjualan Daging</a></li>
             <li><a href="modalpetelurlist.php">Penjualan Telur</a></li>
          </ul>
          <ul class="nav nav-sidebar">
             <li><a href="settingharga.php">Form Setting Harga</a></li>
             <li><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li class="active"><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Form Modal Ayam Potong Periode Ke ...</h1>

          <?php
            $data = $db->query("SELECT periode FROM modal_daging ORDER BY idmodaldaging DESC LIMIT 1")
                      ->fetch_assoc();
            $pecah = explode("-", $data['periode']);
            $periode = $pecah[1]+1;

            if(isset($_POST['btnSubmit'])){
              $hitungdaging = $_POST['f5']*1.8;
              $sql = $db->query("INSERT INTO modal_daging (jumbibitayam, hargabibitayam, hargapakan, 
                        listrik, obat, periode, tanggal, sisadaging) VALUES ('".$_POST['f5']."','".$_POST['f1']."','".$_POST['f2']."','".$_POST['f3']."','".$_POST['f4']."','".$_POST['f0']."','".date('Y-m-d')."', '".$hitungdaging."')");

              if($sql) {
                header('location: modalpotonglist.php');
              }
            }
          ?>

            <form class="form-horizontal" action="modalpotong.php" method="post">
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Periode : </label>
                <div class="col-sm-4"> 
                  <input required name="f0" value="D<?php echo date('Y.m');?>-<?php echo $periode;?>" type="text" class="form-control" Placeholder="periode" id="dengan-rupiah">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="jenis">Jumlah Bibit Ayam : </label>
                <div class="col-sm-4">          
                  <input autofocus required name="f5" type="text" class="form-control" Placeholder="jumlah bibit ayam" id="dengan-rupiah">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Harga Bibit Ayam : </label>
                <div class="col-sm-4"> 
                  <input required name="f1" type="text" class="form-control" Placeholder="input harga bibit ayam" id="dengan-rupiah">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="jenis">Harga Pakan Ayam : </label>
                <div class="col-sm-4">          
                  <input required name="f2" type="text" class="form-control" Placeholder="input harga pakan ayam" id="dengan-rupiah">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="jenis">Biaya Listrik : </label>
                <div class="col-sm-4">          
                  <input required name="f3" type="text" class="form-control" Placeholder="input biaya listrik" id="dengan-rupiah">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="jenis">Harga Obat : </label>
                <div class="col-sm-4">          
                  <input required name="f4" type="text" class="form-control" Placeholder="input harga obat" id="dengan-rupiah">
                </div>
              </div>
              <div class="form-group">        
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" name="btnSubmit" class="btn btn-success">Submit</button>
                  <a href="modalpotonglist.php" class="btn btn-primary">Lihat Modal</a>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php include('libs/foot.php');?>
  </body>
</html>
