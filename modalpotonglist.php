<?php include('config/db.php');?>
<?php 
session_start();
if(empty($_SESSION['idloginuser'])) header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li class="active"><a href="modalpotonglist.php">Penjualan Daging</a></li>
             <li><a href="modalpetelurlist.php">Penjualan Telur</a></li>
          </ul>
          <ul class="nav nav-sidebar">
             <li><a href="settingharga.php">Form Setting Harga</a></li>
             <li><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
          </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">List Modal Ayam Potong</h1>

          <?php
          $query = $db->query("SELECT * FROM modal_daging");
          while($dd = $query->fetch_assoc()){?>
          <div class="col-sm-4">
            <div class="panel panel-primary">
              <div class="panel-body">
                <h3>Periode <b><?php echo $dd['periode'];?></b></h3>
                <table class="table table-bordered">
                  <tr>
                    <td>Jumlah Bibit Ayam</td>
                    <td><b><?php echo $dd['jumbibitayam'];?></b> ekor</td>
                  </tr>
                  <tr>
                    <td>Harga Bibit Ayam</td>
                    <td><b><?php echo uangIndo($dd['hargabibitayam']);?></b>/ekor</td>
                  </tr>
                  <tr>
                    <td>Pakan Ayam</td>
                    <td><b><?php echo uangIndo($dd['hargapakan']);?></b></td>
                  </tr>
                  <tr>
                    <td>Biaya Obat</td>
                    <td><b><?php echo uangIndo($dd['obat']);?></b></td>
                  </tr>
                  <tr>
                    <td>Biaya Listrik</td>
                    <td><b><?php echo uangIndo($dd['listrik']);?></b></td>
                  </tr>
                  <tr>
                    <td>Total Modal</td>
                    <?php $hitungtotalmodal = $dd['listrik']+$dd['obat']+$dd['hargapakan']+($dd['hargabibitayam']*$dd['jumbibitayam']); ?>
                    <td><b><?php echo uangIndo($hitungtotalmodal);?></b></td>
                  </tr>
                  <tr>
                    <td>Daging Saat Ini</td>
                    <td><b><?php echo $dd['sisadaging'];?></b> kg</td>
                  </tr>
                </table>
                <p>Keuntungan per hari <b><?php echo uangIndo($hitungtotalmodal/30);?></b></p>
                
                <a href="modalpotongtrans.php?periode=<?php echo $dd['periode'];?>" class="btn btn-success">Pemasukan</a>
                <a href="modalpotongrafik.php?periode=<?php echo $dd['periode'];?>" class="btn btn-primary">
                  Grafik Keuntungan
                </a>
              </div>
            </div>
          </div>
          <?php } ?>
        </div>
      </div>
    </div>

    <?php include('libs/foot.php');?>
  </body>
</html>
