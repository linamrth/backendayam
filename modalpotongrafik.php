<?php include('config/db.php');?>
<?php 
session_start();
if(empty($_SESSION['idloginuser'])) header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li class="active"><a href="modalpotonglist.php">Penjualan Daging</a></li>
             <li><a href="modalpetelurlist.php">Penjualan Telur</a></li>
          </ul>
          <ul class="nav nav-sidebar">
             <li><a href="settingharga.php">Form Setting Harga</a></li>
             <li><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
          </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2 class="page-header">
            <a href="modalpotonglist.php" class="btn btn-sm btn-primary">
              <i class="glyphicon glyphicon-backward"></i> 
              back
            </a>
            Ayam Potong Periode : <b><?php echo $_GET['periode'];?></b>
          </h2>

          <div class="panel panel-primary">
            <div class="panel-body">
              <canvas id="myGrafikPotong" height="150px"></canvas>
            </div>
          </div>

          <div class="panel panel-primary">
            <div class="panel-body">
              <h4>Transaksi Penjualan</h4>
              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>ID Transaksi</th>
                    <th>Atas Nama</th>
                    <th>Tanggal</th>
                    <th>Produk</th>
                    <th>Total Rupiah</th>
                    <th>Total Kg</th>
                  </tr>
                </thead>
                <tbody>
                <?php
                $sql1 = $db->query("SELECT t.*, d.* 
                    FROM transaksi t JOIN transaksidetail d 
                    ON t.idtransaksi = d.idtransaksi
                    WHERE d.periode = '".$_GET['periode']."'
                  ");
                $totRp = 0;
                $totKg = 0;
                ?>
                <?php $n=0; while($dd = $sql1->fetch_assoc()){ $n++; 
                  $totRp += $dd['total'];
                  $totKg += $dd['jumkilo'];
                  ?>
                  <tr>
                    <td><?php echo $n;?></td>
                    <td><?php echo $dd['idtransaksi'];?></td>
                    <td><?php echo $dd['atasnama'];?></td>
                    <td><?php echo tglIndo($dd['tanggal']);?></td>
                    <td><?php echo $dd['jenis'];?></td>
                    <td><?php echo $dd['jumkilo'];?> kg</td>
                    <td><?php echo uangIndo($dd['total']);?></td>
                  </tr>
                <?php } ?>
                </tbody>
              </table>
              <p>Total Kg : <b><?php echo $totKg;?></b> kg</p>
              <p>Total Rupiah : <b><?php echo uangIndo($totRp);?></b></p>
            </div>
          </div>         

        </div>

    <?php
    $q1 = $db->query("SELECT t.tanggal, SUM(d.subtotal) as subtotal FROM transaksi t JOIN transaksidetail d 
            ON d.idtransaksi = t.idtransaksi 
            WHERE d.periode = '".$_GET['periode']."'
            GROUP BY DATE_FORMAT(t.tanggal, '%Y-%m-%d')");
    $label1 = array();
    $data1 = array();
    $warna1 = array();
    while ($d1 = $q1->fetch_assoc()) {
      array_push($data1, $d1['subtotal']);

      $randomr    = rand(100, 254);
      $randomg    = rand(100, 254);
      $randomb    = rand(100, 254);
      $warna1     = 'rgba('.$randomr.', '.$randomg.', '.$randomb.', 1)';

      $pecah = explode(" ", $d1['tanggal']);
      array_push($label1, $pecah[0]);
    }
    ?>

    <?php include('libs/foot.php');?>
    <script type="text/javascript">
      var ctx = document.getElementById("myGrafikPotong").getContext('2d');
      var myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: <?php echo json_encode($label1);?>,
          datasets: [{
            label: '<?php echo $_GET['periode'];?>',
            data: <?php echo json_encode($data1);?>,
            fill: false,
            borderWidth: 5,
            borderColor: <?php echo json_encode($warna1);?>,
            backgroundColor: <?php echo json_encode($warna1);?>
          }]
        },
        options: {
          responsive: true,
          hoverMode: 'index',
          title:{
            display: true,
            text:'Penjualan Daging Ayam Potong Periode <?php echo $_GET["periode"];?>'
          },
          scales: {
            yAxes: [{
              type: 'linear',
              display: true,
              ticks: {
                beginAtZero:true
              },
            }]
          }
        }
      });
    </script>
  </body>
</html>
