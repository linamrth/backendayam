<?php include('config/db.php');?>
<?php 
session_start();
if(empty($_SESSION['idloginuser'])) header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li><a href="modalpotonglist.php">Penjualan Daging</a></li>
             <li><a href="modalpetelurlist.php">Penjualan Telur</a></li>
          </ul>
          <ul class="nav nav-sidebar">
             <li><a href="settingharga.php">Form Setting Harga</a></li>
             <li class="active"><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Form Pegawai</h1>

          <?php
          if(isset($_POST['btnSubmit'])){
            if(empty($_POST['idnya'])){
              $sql = $db->query("INSERT INTO pegawai (nama, umur, jeniskelamin, alamat, notelepon) 
              VALUES ('".$_POST['f1']."','".$_POST['f2']."','".$_POST['f3']."','".$_POST['f4']."','".$_POST['f5']."')");
            }else{
              $sql = $db->query("UPDATE pegawai SET nama = '".$_POST['f1']."', umur = '".$_POST['f2']."', jeniskelamin = '".$_POST['f3']."' , alamat = '".$_POST['f4']."', notelepon = '".$_POST['f5']."' WHERE idpegawai = '".$_POST['idnya']."'");
            }
            if($sql) {
              echo "<div class='alert alert-success'>Edit data pegawai sukses.</div>";
            }
          }

          if(isset($_GET['act'])){
            if($_GET['act'] == 'edit') {
              $data = $db->query("SELECT * FROM pegawai WHERE idpegawai = '".$_GET['id']."'")
                      ->fetch_assoc();

            }elseif($_GET['act'] == 'hapus'){
              $sql = $db->query("DELETE FROM pegawai WHERE idpegawai = '".$_GET['id']."'");
              if($sql) {
                echo "<div class='alert alert-success'>Hapus data pegawai sukses.</div>";
              }
            } else {
              $data = array('idpegawai'=>'','nama'=>'','umur'=>'','jeniskelamin'=>'','alamat'=>'','notelepon'=>'');  
            }
          } else {
            $data = array('idpegawai'=>'','nama'=>'','umur'=>'','jeniskelamin'=>'','alamat'=>'','notelepon'=>'');
          }
          ?>

            <form class="form-horizontal" action="pegawai.php" method="post">
              <input type="hidden" value="<?php echo $data['idpegawai'];?>" name="idnya">
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Nama : </label>
                <div class="col-sm-4"> 
                  <input value="<?php echo $data['nama'];?>" required type="text" class="form-control" Placeholder="input nama pegawai" id="nama" name="f1">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Umur : </label>
                <div class="col-sm-4"> 
                  <input value="<?php echo $data['umur'];?>" required type="text" class="form-control" Placeholder="input umur pegawai" id="nama" name="f2">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Jenis Kelamin : </label>
                <div class="col-sm-4"> 
                  <input value="<?php echo $data['jeniskelamin'];?>" required type="text" class="form-control" Placeholder="input jenis kelamin pegawai" id="nama" name="f3">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Alamat : </label>
                <div class="col-sm-4"> 
                  <input value="<?php echo $data['alamat'];?>" required type="text" class="form-control" Placeholder="input alamat pegawai" id="nama" name="f4">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">No Telepon : </label>
                <div class="col-sm-4"> 
                  <input value="<?php echo $data['notelepon'];?>" required type="text" class="form-control" Placeholder="input no telepon pegawai" id="nama" name="f5">
                </div>
              </div>
              <div class="form-group">        
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" name="btnSubmit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>

            <hr>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama</th>
                    <th>Umur</th>
                    <th>Jenis Kelamin</th>
                    <th>Alamat</th>
                    <th>No Telepon</th>
                  </thead>
                  <tbody>
                    <?php
                    $n=0;
                    $sql = $db->query("SELECT * FROM pegawai");
                    while($ss = $sql->fetch_assoc()){ $n++;?>
                    <tr>
                      <td><?php echo $n;?></td>
                      <td><?php echo $ss['nama'];?></td>
                      <td><?php echo $ss['umur'];?></td>
                      <td><?php echo $ss['jeniskelamin'];?></td>
                      <td><?php echo $ss['alamat'];?></td>
                      <td><?php echo $ss['notelepon'];?></td>
                      <td>
                        <a href="pegawai.php?act=edit&id=<?php echo $ss['idpegawai'];?>" class="btn btn-sm btn-primary">edit</a>
                        <a href="pegawai.php?act=hapus&id=<?php echo $ss['idpegawai'];?>" class="btn btn-sm btn-danger">hapus</a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php include('libs/foot.php');?>
  </body>
</html>
