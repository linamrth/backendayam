<?php include('config/db.php');?>
<?php 
session_start();
if(empty($_SESSION['idloginuser'])) header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li><a href="modalpotonglist.php">Penjualan Daging</a></li>
             <li><a href="modalpetelurlist.php">Penjualan Telur</a></li>
          </ul>
          <ul class="nav nav-sidebar">
             <li><a href="settingharga.php">Form Setting Harga</a></li>
             <li><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
          </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h2>Profile Pegawai</h2>
          <br>

          <?php $qq = $db->query("SELECT l.*, p.* FROM pegawai p JOIN pegawailogin l ON p.idpegawai = l.idpegawai WHERE p.idpegawai = '".$_SESSION['idloginuser']."'")->fetch_assoc();?>
          <h3>Informasi Diri</h3>
          <table class="table table-bordered">
            <tr>
              <td>ID</td>
              <td><b><?php echo $qq['idpegawai'];?></b></td>
            </tr>
            <tr>
              <td>Nama</td>
              <td><b><?php echo $qq['nama'];?></b></td>
            </tr>
            <tr>
              <td>Umur</td>
              <td><b><?php echo $qq['umur'];?></b></td>
            </tr>
            <tr>
              <td>No Telepon</td>
              <td><b><?php echo $qq['notelepon'];?></b></td>
            </tr>
            <tr>
              <td>Jenis Kelamin</td>
              <td><b><?php echo $qq['jeniskelamin'];?></b></td>
            </tr>
            <tr>
              <td>Alamat</td>
              <td><b><?php echo $qq['alamat'];?></b></td>
            </tr>
          </table>

          <h3>Informasi Akun</h3>
          <table class="table table-bordered">
            <tr>
              <td>Username</td>
              <td><b><?php echo $qq['username'];?></b></td>
            </tr>
            <tr>
              <td>Password</td>
              <td><b><?php echo $qq['password'];?></b></td>
            </tr>
          </table>
        </div >


    <?php include('libs/foot.php');?>    
  </body>
</html>
