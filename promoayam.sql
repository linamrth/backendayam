-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 02, 2018 at 07:04 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `promoayam`
--

-- --------------------------------------------------------

--
-- Table structure for table `jenis_produk`
--

CREATE TABLE IF NOT EXISTS `jenis_produk` (
  `idjenisproduk` int(11) NOT NULL AUTO_INCREMENT,
  `namaproduk` varchar(20) NOT NULL,
  `hargaproduk` double NOT NULL,
  `kategori` enum('T','D') NOT NULL,
  PRIMARY KEY (`idjenisproduk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `jenis_produk`
--

INSERT INTO `jenis_produk` (`idjenisproduk`, `namaproduk`, `hargaproduk`, `kategori`) VALUES
(1, 'Telur', 20400, 'T'),
(2, 'Ayam Afkir', 35000, 'T'),
(3, 'Ayam Utuh', 29500, 'D'),
(4, 'Ayam Potong', 27000, 'D'),
(5, 'Ayam Pejantan', 30000, 'D'),
(6, 'Paha Ayam', 29000, 'D'),
(7, 'Dada Ayam', 29000, 'D'),
(8, 'Daging Fillet', 38000, 'D'),
(9, 'Sayap Ayam', 29000, 'D'),
(10, 'Punggung Ayam', 19000, 'D'),
(11, 'Kepala Ayam', 8000, 'D'),
(12, 'Kaki Ayam', 21000, 'D'),
(13, 'Usus Ayam', 18000, 'D'),
(14, 'Ati Ampela', 30000, 'D'),
(15, 'Kulit Ayam', 16000, 'D'),
(16, 'Jantung Ayam', 16000, 'D'),
(17, 'Tulang Ayam', 8000, 'D');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_create` datetime DEFAULT NULL,
  `fullname` varchar(30) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `message` varchar(300) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `modal_daging`
--

CREATE TABLE IF NOT EXISTS `modal_daging` (
  `idmodaldaging` int(11) NOT NULL AUTO_INCREMENT,
  `jumbibitayam` int(11) NOT NULL,
  `hargabibitayam` double NOT NULL,
  `hargapakan` double NOT NULL,
  `listrik` double NOT NULL,
  `obat` double NOT NULL,
  `periode` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `sisadaging` int(11) NOT NULL,
  PRIMARY KEY (`idmodaldaging`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `modal_daging`
--

INSERT INTO `modal_daging` (`idmodaldaging`, `jumbibitayam`, `hargabibitayam`, `hargapakan`, `listrik`, `obat`, `periode`, `tanggal`, `sisadaging`) VALUES
(4, 300, 7000, 3000000, 200000, 1000000, 'D2017.12-4', '2017-12-30', 515),
(5, 100, 7000, 1000000, 200000, 330000, 'D2017.12-5', '2017-12-30', 135);

-- --------------------------------------------------------

--
-- Table structure for table `modal_telur`
--

CREATE TABLE IF NOT EXISTS `modal_telur` (
  `idmodaltelur` int(11) NOT NULL AUTO_INCREMENT,
  `jumbibitayam` int(11) NOT NULL,
  `hargabibitayam` double NOT NULL,
  `hargapakan` double NOT NULL,
  `periode` varchar(20) NOT NULL,
  `tanggal` date NOT NULL,
  `sisatelur` int(11) NOT NULL,
  PRIMARY KEY (`idmodaltelur`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `modal_telur`
--

INSERT INTO `modal_telur` (`idmodaltelur`, `jumbibitayam`, `hargabibitayam`, `hargapakan`, `periode`, `tanggal`, `sisatelur`) VALUES
(4, 300, 50000, 2880000, 'T2017.12-4', '2017-12-01', 495),
(5, 100, 50000, 960000, 'T2017.12-5', '2017-11-01', 100),
(6, 100, 50000, 960000, 'T2017.12-6', '2017-12-30', 34);

-- --------------------------------------------------------

--
-- Table structure for table `pegawai`
--

CREATE TABLE IF NOT EXISTS `pegawai` (
  `idpegawai` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `umur` int(11) NOT NULL,
  `jeniskelamin` enum('L','P') NOT NULL,
  `alamat` varchar(50) NOT NULL,
  `notelepon` varchar(15) NOT NULL,
  PRIMARY KEY (`idpegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `pegawai`
--

INSERT INTO `pegawai` (`idpegawai`, `nama`, `umur`, `jeniskelamin`, `alamat`, `notelepon`) VALUES
(5, 'Lina Meritha', 20, 'P', 'Jl Kutisari Selatan Gg IVa no.25', '083849317760'),
(6, 'Ahmad Ardiansyah', 21, 'L', 'Jl Sememu Ketewel Pasirian Lumajang', '082849317760');

-- --------------------------------------------------------

--
-- Table structure for table `pegawailogin`
--

CREATE TABLE IF NOT EXISTS `pegawailogin` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idpegawai` int(11) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `last_login` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idpegawai` (`idpegawai`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pegawailogin`
--

INSERT INTO `pegawailogin` (`id`, `idpegawai`, `username`, `password`, `last_login`) VALUES
(1, 6, 'ardiansyah', 'ardiansyah', '2018-01-02 18:27:05');

-- --------------------------------------------------------

--
-- Table structure for table `transaksi`
--

CREATE TABLE IF NOT EXISTS `transaksi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idtransaksi` varchar(20) NOT NULL,
  `atasnama` varchar(50) NOT NULL,
  `tanggal` datetime NOT NULL,
  `idpegawai` int(11) NOT NULL,
  `total` double NOT NULL,
  `totalkg` int(11) NOT NULL,
  `bayar` double NOT NULL,
  `kembali` double NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `transaksi`
--

INSERT INTO `transaksi` (`id`, `idtransaksi`, `atasnama`, `tanggal`, `idpegawai`, `total`, `totalkg`, `bayar`, `kembali`) VALUES
(1, 'DT20171230-6', 'Lina Meritha', '2017-12-30 18:58:37', 6, 204000, 10, 210000, 6000),
(2, 'DT20171230-7', 'Ardiansyah', '2017-12-30 19:00:07', 6, 408000, 20, 410000, 2000),
(3, 'DT20171230-8', 'Wahyu Izzudin', '2017-12-30 19:07:00', 6, 435000, 15, 450000, 15000),
(4, 'DT20171230-9', 'Dimas Andre DW', '2017-12-30 19:15:05', 6, 1330000, 35, 1350000, 20000),
(5, 'DT20171230-10', 'Arif S', '2017-12-29 19:45:23', 6, 102000, 5, 110000, 8000),
(6, 'DT20171230-11', 'Ariel', '2017-12-29 19:45:48', 6, 102000, 5, 110000, 8000),
(7, 'DT20171230-12', 'Didin', '2017-12-28 20:52:52', 6, 244800, 12, 250000, 5200),
(8, 'DT20171230-13', 'Dudun', '2017-12-28 20:56:14', 6, 448800, 22, 450000, 1200),
(9, 'DT20171230-14', 'Udin Sedunia', '2017-12-27 21:03:15', 6, 367200, 18, 400000, 32800),
(10, 'DT20171230-15', 'Sudiro Tora', '2017-12-27 21:07:29', 6, 673200, 33, 680000, 6800),
(11, 'DT20171230-16', 'Mb. Linthung', '2017-12-27 21:25:16', 6, 1020000, 50, 1020000, 0),
(12, 'DT20171230-17', 'Diniatul Umami', '2017-12-28 21:26:55', 6, 428400, 21, 430000, 1600),
(13, 'DT20171230-18', 'Ando', '2017-12-29 21:27:47', 6, 1020000, 50, 1020000, 0),
(14, 'DT20171230-19', 'Ali Rodhi', '2017-12-30 21:28:17', 6, 510000, 25, 510000, 0),
(15, 'DT20171230-20', 'Sugianto', '2017-12-29 21:57:13', 6, 290000, 10, 290000, 0),
(16, 'DT20171230-21', 'Sugiarti', '2017-12-29 21:57:34', 6, 380000, 10, 380000, 0);

-- --------------------------------------------------------

--
-- Table structure for table `transaksidetail`
--

CREATE TABLE IF NOT EXISTS `transaksidetail` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `idtransaksi` varchar(20) NOT NULL,
  `jenis` varchar(30) NOT NULL,
  `harga` double NOT NULL,
  `jumkilo` int(11) NOT NULL,
  `subtotal` double NOT NULL,
  `periode` varchar(10) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idtransaksi` (`idtransaksi`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `transaksidetail`
--

INSERT INTO `transaksidetail` (`id`, `idtransaksi`, `jenis`, `harga`, `jumkilo`, `subtotal`, `periode`) VALUES
(24, 'DT20171230-6', 'Telur', 20400, 10, 204000, 'T2017.12-4'),
(25, 'DT20171230-7', 'Telur', 20400, 20, 408000, 'T2017.12-5'),
(28, 'DT20171230-8', 'Paha Ayam', 29000, 10, 290000, 'D2017.12-4'),
(29, 'DT20171230-8', 'Dada Ayam', 29000, 5, 145000, 'D2017.12-4'),
(30, 'DT20171230-9', 'Daging Fillet', 38000, 35, 1330000, 'D2017.12-5'),
(31, 'DT20171230-10', 'Telur', 20400, 5, 102000, 'T2017.12-4'),
(32, 'DT20171230-11', 'Telur', 20400, 5, 102000, 'T2017.12-5'),
(36, 'DT20171230-12', 'Telur', 20400, 12, 244800, 'T2017.12-4'),
(37, 'DT20171230-13', 'Telur', 20400, 22, 448800, 'T2017.12-5'),
(38, 'DT20171230-14', 'Telur', 20400, 18, 367200, 'T2017.12-4'),
(39, 'DT20171230-15', 'Telur', 20400, 33, 673200, 'T2017.12-5'),
(40, 'DT20171230-16', 'Telur', 20400, 50, 1020000, 'T2017.12-6'),
(41, 'DT20171230-17', 'Telur', 20400, 21, 428400, 'T2017.12-6'),
(42, 'DT20171230-18', 'Telur', 20400, 50, 1020000, 'T2017.12-6'),
(43, 'DT20171230-19', 'Telur', 20400, 25, 510000, 'T2017.12-6'),
(44, 'DT20171230-20', 'Dada Ayam', 29000, 10, 290000, 'D2017.12-4'),
(45, 'DT20171230-21', 'Daging Fillet', 38000, 10, 380000, 'D2017.12-5');

-- --------------------------------------------------------

--
-- Table structure for table `usersign`
--

CREATE TABLE IF NOT EXISTS `usersign` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_create` datetime DEFAULT NULL,
  `firstname` varchar(15) DEFAULT NULL,
  `lastname` varchar(15) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(100) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `usersign`
--

INSERT INTO `usersign` (`id`, `date_create`, `firstname`, `lastname`, `email`, `password`, `last_login`) VALUES
(1, '2017-12-27 11:04:16', 'Ahmad', 'Ardiansyah', 'ardiansyah3ber@gmail.com', 'terLalu7', NULL);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pegawailogin`
--
ALTER TABLE `pegawailogin`
  ADD CONSTRAINT `pegawailogin_ibfk_1` FOREIGN KEY (`idpegawai`) REFERENCES `pegawai` (`idpegawai`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
