<?php include('config/db.php');?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li><a href="modalpotonglist.php">Penjualan Daging</a></li>
             <li><a href="modalpetelurlist.php">Penjualan Telur</a></li>
          </ul>
          <ul class="nav nav-sidebar">
             <li class="active"><a href="settingharga.php">Form Setting Harga</a></li>
             <li><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
          </ul>
        </div>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Form Setting Harga Produk</h1>

          <?php
          if(isset($_POST['btnSubmit'])){
            if(empty($_POST['idnya'])){
              $sql = $db->query("INSERT INTO jenis_produk (namaproduk, hargaproduk, kategori) 
              VALUES ('".$_POST['f2']."','".$_POST['f3']."','".$_POST['f1']."')");
            }else{
              $sql = $db->query("UPDATE jenis_produk SET namaproduk = '".$_POST['f2']."', hargaproduk = '".$_POST['f3']."', kategori = '".$_POST['f1']."' WHERE idjenisproduk = '".$_POST['idnya']."'");
            }
            if($sql) {
              echo "<div class='alert alert-success'>Edit jenis produk sukses.</div>";
            }
          }

          if(isset($_GET['act'])){
            if($_GET['act'] == 'edit') {
              $data = $db->query("SELECT * FROM jenis_produk WHERE idjenisproduk = '".$_GET['id']."'")
                      ->fetch_assoc();

            }elseif($_GET['act'] == 'hapus'){
              $sql = $db->query("DELETE FROM jenis_produk WHERE idjenisproduk = '".$_GET['id']."'");
              if($sql) {
                echo "<div class='alert alert-success'>Hapus jenis produk sukses.</div>";
              }
            } else {
              $data = array('idjenisproduk'=>'','namaproduk'=>'','hargaproduk'=>'','kategori'=>'');  
            }
          } else {
            $data = array('idjenisproduk'=>'','namaproduk'=>'','hargaproduk'=>'','kategori'=>'');
          }
          ?>

            <form class="form-horizontal" action="settingharga.php" method="post">
              <input type="hidden" value="<?php echo $data['idjenisproduk'];?>" name="idnya">
              <div class="form-group">
                <label class="control-label col-sm-2" for="jenis">Kategori : </label>
                <div class="col-sm-4">          
                  <select required name="f1" class="form-control" id="sel1">
                    <?php if(isset($_GET['act'])){ ?>
                    <option value="<?php echo $data['kategori'];?>"><?php echo jenisAyam($data['kategori']);?></option>
                    <?php } else { ?>
                    <option value="">-pilih-</option>
                    <?php } ?>
                    <option value="T">Ayam Petelur</option>
                    <option value="D">Ayam Potong</option>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="jenis">Jenis Produk : </label>
                <div class="col-sm-4">          
                  <input value="<?php echo $data['namaproduk'];?>" required type="text" name="f2" class="form-control" placeholder="jenis produk">
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="jenis">Harga Per Kilo : </label>
                <div class="col-sm-4">          
                  <input value="<?php echo $data['hargaproduk'];?>" required type="text" name="f3" class="form-control" Placeholder="harga per kilo" id="hargaperkilo">
                </div>
              </div>
              <div class="form-group">        
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" name="btnSubmit" class="btn btn-success">Submit</button>
                </div>
              </div>
            </form>

            <hr>
            <div class="table-responsive">
              <table class="table table-striped">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Kategori</th>
                    <th>Jenis Produk</th>
                    <th>Harga per kilo</th>
                    <th></th>
                  </thead>
                  <tbody>
                    <?php
                    $n=0;
                    $sql = $db->query("SELECT * FROM jenis_produk");
                    while(($ss = $sql->fetch_assoc())!==null){ $n++;?>
                    <tr>
                      <td><?php echo $n;?></td>
                      <td><?php echo jenisAyam($ss['kategori']);?></td>
                      <td><?php echo $ss['namaproduk'];?></td>
                      <td><?php echo uangIndo($ss['hargaproduk']);?></td>
                      <td>
                        <a href="settingharga.php?act=edit&id=<?php echo $ss['idjenisproduk'];?>" class="btn btn-sm btn-primary">edit</a>
                        <a href="settingharga.php?act=hapus&id=<?php echo $ss['idjenisproduk'];?>" class="btn btn-sm btn-danger">hapus</a>
                      </td>
                    </tr>
                    <?php } ?>
                  </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php include('libs/foot.php');?>
  </body>
</html>
