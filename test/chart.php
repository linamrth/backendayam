<?php
$db = new mysqli("localhost","root","","dbvalas4");

/******************************************************************************/
$qjenis = $db->query("
	SELECT 
		t.tgl_transaksi, 
		n.jenis_valas, 
		sum(n.jumlah) as jmlvalas    
	FROM 
		transaksi t, 
		nota n 
	WHERE 
		(t.tgl_transaksi BETWEEN '2017-07-06' AND '2017-07-20') AND
		t.nonota = n.nonota AND
		n.jenis_valas = 'USD'
	GROUP BY 
		t.tgl_transaksi
");
$djenis = array();
$dnilai = array();
$warna0  = array();
while($dd = $qjenis->fetch_assoc()){
	$randomr  = rand(100, 255);
	$randomg  = rand(100, 255);
	$randomb  = rand(100, 255);
	$warna 		= 'rgba('.$randomr.', '.$randomg.', '.$randomb.', 1)';
	array_push($djenis, $dd['tgl_transaksi']);
	array_push($dnilai, $dd['jmlvalas']);
	array_push($warna0, $warna);
}
/******************************************************************************/

/******************************************************************************/
$qvalas1 = $db->query("
	SELECT 
		t.tgl_transaksi, 
		n.jenis_valas, 
		sum(n.jumlah) as jmlvalas 
	FROM 
		transaksi t, 
		nota n 
	WHERE 
		t.tgl_transaksi = '2017-08-02' AND
		t.nonota = n.nonota AND
		t.jenis = 'B'
	GROUP BY 
		n.jenis_valas
");
$dvalas1 = array();
$dtotal1 = array();
$warna1  = array();
while($dd = $qvalas1->fetch_assoc()) { 
	$randomr  	= rand(100, 254);
	$randomg  	= rand(100, 254);
	$randomb  	= rand(100, 254);
	$warna 		= 'rgba('.$randomr.', '.$randomg.', '.$randomb.', 1)';
	array_push($dvalas1, $dd['jenis_valas']); 
	array_push($dtotal1, $dd['jmlvalas']); 
	array_push($warna1, $warna);
}
/******************************************************************************/

/******************************************************************************/
$qvalas2 = $db->query("
	SELECT 
		t.tgl_transaksi, 
		n.jenis_valas, 
		sum(n.jumlah) as jmlvalas 
	FROM 
		transaksi t, 
		nota n 
	WHERE 
		t.tgl_transaksi = '2017-07-17s' AND
		t.nonota = n.nonota AND
		t.jenis = 'J'
	GROUP BY 
		n.jenis_valas
");
$dvalas2 = array();
$dtotal2 = array();
$warna2  = array();
while($dd = $qvalas2->fetch_assoc()) { 
	$randomr  	= rand(100, 254);
	$randomg  	= rand(100, 254);
	$randomb  	= rand(100, 254);
	$warna 		= 'rgba('.$randomr.', '.$randomg.', '.$randomb.', 1)';
	array_push($dvalas2, $dd['jenis_valas']); 
	array_push($dtotal2, $dd['jmlvalas']); 
	array_push($warna2, $warna); 
}
/******************************************************************************/

?>
<html>
<head>
	<title>ChartJS</title>
	<script src="js/chartjs/Chart.bundle.js" type="text/javascript"></script>
	<script src="js/chartjs//Chart.bundle.min.js" type="text/javascript"></script>
	<script src="js/chartjs//Chart.js" type="text/javascript"></script>
	<script src="js/chartjs//Chart.min.js" type="text/javascript"></script>
</head>
<body>
	<canvas id="myJenis" height="100px"></canvas>
	<hr>
	<canvas id="myBeli" height="100px"></canvas>
	<hr>
	<canvas id="myJual" height="100px"></canvas>
	<hr>
	<canvas id="myChart" height="100px"></canvas>
	
	<script>
	var beli = document.getElementById("myJenis").getContext('2d');
	var myChart = new Chart(beli, {
		type: 'line',
		data: {
			labels: <?php echo json_encode($djenis);?>,
			datasets: [{
				label: 'USD ',
				data: <?php echo json_encode($dnilai);?>,
				borderWidth: 5,
				backgroundColor: 'rgba(100,0,255)',
				borderColor: 'rgba(100,0,255)',
				fill: false
			}]
		},
		options: {
			scales: {
				yAxes: [{
					stacked: true
				}]
			},
			elements: {
				line: {
					tension: 0, // disables bezier curves
				}
			}
		}
	});
	
	var ctx = document.getElementById("myChart").getContext('2d');
	var myChart = new Chart(ctx, {
		type: 'bar',
		data: {
			labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
			datasets: [{
				label: 'Tutorials',
				data: [12, 19, 3, 5, 2, 3],
				borderWidth: 1,
				backgroundColor: [
					'rgba(0, 200, 0, 1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)'
				],
				borderColor: [
					'rgba(0,270,0,1)',
					'rgba(54, 162, 235, 1)',
					'rgba(255, 206, 86, 1)',
					'rgba(75, 192, 192, 1)',
					'rgba(153, 102, 255, 1)',
					'rgba(255, 159, 64, 1)'
				],
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});

	var beli = document.getElementById("myBeli").getContext('2d');
	var myChart = new Chart(beli, {
		type: 'bar',
		data: {
			labels: <?php echo json_encode($dvalas1);?>,
			datasets: [{
				label: 'Pembelian ',
				data: <?php echo json_encode($dtotal1);?>,
				borderWidth: 1,
				backgroundColor: <?php echo json_encode($warna1); ?>,
				borderColor: <?php echo json_encode($warna1); ?>,
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});
	
	var jual = document.getElementById("myJual").getContext('2d');
	var myChart = new Chart(jual, {
		type: 'bar',
		data: {
			labels: <?php echo json_encode($dvalas2);?>,
			datasets: [{
				label: 'Penjualan ',
				data: <?php echo json_encode($dtotal2);?>,
				borderWidth: 1,
				borderColor: <?php echo json_encode($warna2);?>,
				backgroundColor: <?php echo json_encode($warna2);?>
			}]
		},
		options: {
			scales: {
				yAxes: [{
					ticks: {
						beginAtZero:true
					}
				}]
			}
		}
	});
	</script>
</body>
</html>