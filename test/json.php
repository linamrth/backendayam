<?php
$db = new mysqli("localhost","root","ardi","promoayam");

$data = array();
$sql0 = $db->query("SELECT * FROM modal_daging");
while ($dd0 = $sql0->fetch_assoc()) {

	$dt = array();

	$sql1 = $db->query("SELECT SUM(d.subtotal) as total, t.tanggal
				FROM transaksidetail d JOIN transaksi t ON t.idtransaksi = d.idtransaksi 
				WHERE t.tanggal BETWEEN '2017-12-25%' AND '2017-12-31%' AND d.periode = '".$dd0['periode']."'
				GROUP BY DATE_FORMAT(t.tanggal, '%Y-%m-%d')
			");

	while ($dd1 = $sql1->fetch_assoc()) {
		if(count($dd1['total']) == 0){
			$dt[] = 0;
		} else {
			$dt[] = $dd1['total'];
		}
	}


	array_push($data, array(
			'label'=>$dd0['periode'], 
			'borderColor'=>'blue',
			'backroundColor'=>'lightblue',
			'fill'=>false,
			'data'=>$dt
		)
	);
}

$label = array();
$sql2 = $db->query("SELECT SUM(d.subtotal) as total, t.tanggal
				FROM transaksidetail d JOIN transaksi t ON t.idtransaksi = d.idtransaksi 
				WHERE t.tanggal BETWEEN '2017-12-25%' AND '2017-12-31%' AND d.periode LIKE 'D%'
				GROUP BY DATE_FORMAT(t.tanggal, '%Y-%m-%d')");
while ($dd = $sql2->fetch_assoc()) {
	array_push($label, $dd['tanggal']);
}

header('content-type: application/json');
echo json_encode($data);
echo "<br>";
echo json_encode($label);