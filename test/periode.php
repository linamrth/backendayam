<?php include('config/db.php');?>
<?php 
session_start();
if(empty($_SESSION['idloginuser'])) header('location: login.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="logopitik.png">

    <title>Warung Bejan</title>

    <?php include('libs/head.php');?>
  </head>

  <body>

    <?php include('libs/menu.php');?>

    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar">
             <li><a href="settingharga.php">Form Setting Harga</a></li>
             <li><a href="pegawai.php">Form Pegawai</a></li>
          </ul>
          <ul class="nav nav-sidebar">
            <li class="active"><a href="periode.php">Form Periode</a></li>
            <li><a href="modalpetelur.php">Form Modal Ayam Petelur</a></li>
            <li><a href="modalpotong.php">Form Modal Ayam Potong</a></li>
            <li><a href="transaksi.php">Form Transaksi</a></li>
          </ul>
        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
          <h1 class="page-header">Form Periode</h1>

            <form class="form-horizontal" action="/action_page.php">
              <div class="form-group">
                <label class="control-label col-sm-2" for="pwd">Periode Ke : </label>
                <div class="col-sm-4"> 
                  <select class="form-control" id="sel1">
                    <?php for($i=1; $i<101; $i++): ?>
                    <option><?php echo $i;?></option>
                    <?php endfor; ?>
                  </select>
                </div>
              </div>
              <div class="form-group">
                <label class="control-label col-sm-2" for="jenis">Kategori : </label>
                <div class="col-sm-4">          
                  <select class="form-control" id="sel1">
                    <option>Ayam Petelur</option>
                    <option>Ayam Potong</option>
                  </select>
                </div>
              </div>
              <div class="form-group">        
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Input Modal</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>

    <?php include('libs/foot.php');?>
  </body>
</html>
